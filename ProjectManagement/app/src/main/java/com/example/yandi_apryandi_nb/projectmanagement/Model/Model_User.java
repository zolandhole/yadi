package com.example.yandi_apryandi_nb.projectmanagement.Model;

public class Model_User {
    public String id_user;
    public String username;
    public String password;
    public String nama_user;
    public String status;

    public Model_User(){

    }

    public void setId_user(String id_user){
        this.id_user = id_user;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setNama_user(String nama_user){
        this.nama_user = nama_user;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getId_user(){
        return id_user;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public String getNama_user(){
        return nama_user;
    }

    public String getStatus(){
        return status;
    }
}
