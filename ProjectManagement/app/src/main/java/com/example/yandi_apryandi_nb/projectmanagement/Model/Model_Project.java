package com.example.yandi_apryandi_nb.projectmanagement.Model;

public class Model_Project {
    public String id_project;
    public String nama_project;
    public String status;
    public String keterangan;

    public Model_Project(String id_project, String nama_project, String status, String keterangan){
        this.id_project = id_project;
        this.nama_project = nama_project;
        this.status = status;
        this.keterangan = keterangan;
    }

    public void setId_project(String id_project){
        this.id_project = id_project;
    }

    public void setNama_project(String nama_project){
        this.nama_project = nama_project;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setKeterangan(String keterangan){
        this.keterangan = keterangan;
    }

    public String getId_project(){
        return id_project;
    }

    public String getNama_project(){
        return nama_project;
    }

    public String getStatus(){
        return status;
    }

    public String getKeterangan(){
        return keterangan;
    }
}
