package com.example.yarud.jjwtaplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

public class MainActivity extends AppCompatActivity {
    Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    String jws = Jwts.builder().setSubject("Joe").signWith(key).compact();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
}
