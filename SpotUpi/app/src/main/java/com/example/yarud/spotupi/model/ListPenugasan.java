package com.example.yarud.spotupi.model;

public class ListPenugasan {
    private String idmk;
    private String kodekls;
    private String namakelas;
    private String kodemk;
    private String namadsn;
    private String namamk;
    private String sks;
    private String namapst;
    private String thn;
    private String smt;

    public ListPenugasan(String idmk, String kodekls, String namakelas, String kodemk, String namadsn, String namamk, String sks, String namapst, String thn, String smt) {
        this.idmk = idmk;
        this.kodekls = kodekls;
        this.namakelas = namakelas;
        this.kodemk = kodemk;
        this.namadsn = namadsn;
        this.namamk = namamk;
        this.sks = sks;
        this.namapst = namapst;
        this.thn = thn;
        this.smt = smt;
    }

    public String getIdmk(){
        return idmk;
    }

    public String getKodekls() {
        return kodekls;
    }

    public String getNamakelas() {
        return namakelas;
    }

    public String getKodemk() {
        return kodemk;
    }

    public String getNamadsn() {
        return namadsn;
    }

    public String getNamamk() {
        return namamk;
    }

    public String getSks() {
        return sks;
    }

    public String getNamapst() {
        return namapst;
    }

    public String getThn() {
        return thn;
    }

    public String getSmt() {
        return smt;
    }
}
