package com.example.yarud.spotupi;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spotupi.constant.Url;
import com.example.yarud.spotupi.helper.PenugasanAdapter;
import com.example.yarud.spotupi.model.DBHandler;
import com.example.yarud.spotupi.model.ListPenugasan;
import com.example.yarud.spotupi.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PenugasanActivity extends AppCompatActivity {

    private final AppCompatActivity activityPenugasan = PenugasanActivity.this;
    private DBHandler dbHandler;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListPenugasan> listPenugasans;
    private AlertDialog myAlertDialog;
    private Button buttonBackPenugasan;
    private String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penugasan);
        recyclerView = findViewById(R.id.RecycleViewPenugasan);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        listPenugasans = new ArrayList<>();
        dbHandler = new DBHandler(activityPenugasan);
        initRunning();
        buttonBackPenugasan = findViewById(R.id.ButtonBackPenugasan);
        buttonBackPenugasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });
    }

    private void initRunning() {
        Intent intentMain = getIntent();
        String kodedosen = intentMain.getStringExtra("KODEDOSEN");
        loadRecycleViewData(kodedosen);
    }

    private void loadRecycleViewData(final String kodedosen) {
        int idUser = dbHandler.getLastId();
        User user = dbHandler.getUser(idUser);
        String username = user.getUsername();
        String password = user.getPassword();
        get_Token(username, password, kodedosen);
    }


    private void alert(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityPenugasan,R.style.todoDialogLight);
        builder.setTitle("")
                .setMessage("Mengambil Data ...!")
                .setCancelable(false);
        myAlertDialog = builder.show();

    }
    private void get_Token(final String nip, final String pass, final String kodedosen) {
        alert();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Url.POST_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            myAlertDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            token = jsonObject.getString("token");
                            get_Penugasan(kodedosen, token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myAlertDialog.dismiss();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("username", nip);
                params.put("password", pass);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void get_Penugasan(final String kodedosen, final String token) {
        alert();
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Url.GET_PENUGASAN + kodedosen,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        myAlertDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String test = jsonObject.getString("dt_penugasan");
                            JSONArray array = jsonObject.getJSONArray("dt_penugasan");
                            for (int i=0; i<array.length(); i++){
                                JSONObject j = array.getJSONObject(i);
                                ListPenugasan penugasan = new ListPenugasan(
                                        j.getString("IDMK"),
                                        j.getString("KODEKLS"),
                                        j.getString("NAMAKELAS"),
                                        j.getString("KODEMK"),
                                        j.getString("NAMADSN"),
                                        j.getString("NAMAMK"),
                                        j.getString("SKS"),
                                        j.getString("NAMAPST"),
                                        j.getString("THN"),
                                        j.getString("SMT")
                                );
                                listPenugasans.add(penugasan);
                                adapter = new PenugasanAdapter(listPenugasans, getApplicationContext());
                                recyclerView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        myAlertDialog.dismiss();
                        Toast.makeText(getApplicationContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("id", kodedosen);
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
