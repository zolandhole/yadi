package com.example.yarud.spotupi.constant;

public class Url {
    public static final String GET_USER = "http://api.upi.edu/jwt/api/user";
    //1. login
    //url: http://api.upi.edu/jwt/api/user/login
    //method: post
    //param: username, password (nip dan password acs)

    public static final String POST_TOKEN = "http://api.upi.edu/jwt/api/user/login";
    //2. kodedsn dan nama dosen
    //url: http://api.upi.edu/jwt/api/user
    //param: Authorization = Bearer (token)
    //method: get

    public static final String GET_LOGOUT = "http://api.upi.edu/jwt/api/logout";
    //3. logout
    //url: http://api.upi.edu/jwt/api/logout
    //param: Authorization = Bearer (token)
    //method: get

    public static final String GET_PENUGASAN = "http://api.upi.edu/jwt/api/penugasan/";
    //4. penugasan
    //url: http://api.upi.edu/jwt/api/penugasan/id
    //param: id=kodedsn
    //param: Authorization = Bearer (token)
    //method: get

    public static final String GET_DETIL_MK = "http://api.upi.edu/jwt/api/detil_mk/";
    //5. detil mk
    //url: http://api.upi.edu/jwt/api/detil_mk/id
    //param: id=idmk
    //param: Authorization = Bearer (token)
    //method: get

    public static final String GET_RISALAH_MK = "http://api.upi.edu/jwt/api/risalah_mk/";
    //6. risalah mk
    //url: http://api.upi.edu/jwt/api/risalah_mk/id
    //param: id=idmk
    //param: Authorization = Bearer (token)
    //method: get

    public static final String GET_PRESENSI = "http://api.upi.edu/jwt/api/presensi/";
    //7. presensi
    //url: http://api.upi.edu/jwt/api/presensi/idrs
    //param: idrs=idrisalah
    //param: Authorization = Bearer (token)
    //method: get

    public static final String POST_INPUT_PRESENSI = "http://api.upi.edu/jwt/api/input_presensi";
    //8. input presensi
    //url: http://api.upi.edu/jwt/api/input_presensi
    //param: 	id_rs= id_risalah (string)
    //	nim = array/string
    //	status = array/string
    //	ket = array/string
    //param: Authorization = Bearer (token)
    //method: post
}
