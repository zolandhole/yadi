package com.example.yarud.spotupi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spotupi.constant.Url;
import com.example.yarud.spotupi.model.DBHandler;
import com.example.yarud.spotupi.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activityMain = MainActivity.this;
    private DBHandler dbHandler;
    private TextView txtUsername, txtGreeting;
    private Button buttonLogout, buttonAbsen, buttonUlangi;
    private ConstraintLayout constraintLayoutGagalKonek;
    private CardView cardViewAbsen;
    private AlertDialog myAlertDialog;
    private ImageView imageViewNoConnection, imageViewLogoupi;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initObjects();
        initViews();
        initListeners();
        initRunning();
    }

    private void initViews(){
        txtUsername= findViewById(R.id.TextViewUsername);
        txtGreeting = findViewById(R.id.TextViewGreeting);
        buttonAbsen = findViewById(R.id.ButtonAbsen);
        buttonLogout = findViewById(R.id.ButtonLogout);
        buttonUlangi = findViewById(R.id.ButtonUlangi);
        cardViewAbsen = findViewById(R.id.CardViewAbsen);
        constraintLayoutGagalKonek = findViewById(R.id.CardViewGagalKonek);
        imageViewNoConnection = findViewById(R.id.ImageViewNoConnection);
        imageViewLogoupi = findViewById(R.id.ImageViewLogoupi);
    }

    private void initListeners(){
        buttonAbsen.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
        buttonUlangi.setOnClickListener(this);
    }

    private void initObjects(){
        dbHandler = new DBHandler(activityMain);
    }

    private void initRunning(){
        if (dbHandler.checkDB()){
            Intent intentLogin = new Intent(activityMain,LoginActivity.class);
            startActivity(intentLogin);
            finish();
        }else {
            getTokenFromDB();
        }
        Intent intentLogin = getIntent();
        String gelarnama = intentLogin.getStringExtra("GELARNAMA");
        txtUsername.setText(gelarnama);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ButtonLogout:
                logoutConfirmation();
                break;
            case R.id.ButtonAbsen:
                toPenugasan();
                break;
            case R.id.ButtonUlangi:
                initRunning();
                break;
        }
    }

    private void toPenugasan() {
        int idUser = dbHandler.getLastId();
        User user = dbHandler.getUser(idUser);
        String kodedosen = user.getKodedosen();
        Intent intentPenugasan = new Intent(activityMain, PenugasanActivity.class);
        intentPenugasan.putExtra("KODEDOSEN", kodedosen);
        startActivity(intentPenugasan);
    }

    private void getTokenFromDB() {
        int idUser = dbHandler.getLastId();
        User user = dbHandler.getUser(idUser);
        String nip = user.getUsername();
        String pass = user.getPassword();
        String kodedosen = user.getKodedosen();
        getToken(nip, pass, kodedosen);
    }
    private void alert(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityMain,R.style.todoDialogLight);
        builder.setTitle("")
                .setMessage("Sedang menghubungkan ke server ...!")
                .setCancelable(false);
        myAlertDialog = builder.show();

    }

    private void getToken(final String username, final String password, final String kodedosen) {
        alert();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Url.POST_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            myAlertDialog.dismiss();
                            visibleAll();
                            JSONObject jsonObject = new JSONObject(response);
                            String dataToken = jsonObject.getString("token");
                            verifyToken(dataToken, username, password,  kodedosen);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myAlertDialog.dismiss();
                        invisibleAll();
                        Toast.makeText(getApplicationContext(),"Koneksi ke Server Gagal",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void invisibleAll() {
        txtUsername.setVisibility(View.INVISIBLE);
        txtGreeting.setVisibility(View.INVISIBLE);
        buttonAbsen.setVisibility(View.INVISIBLE);
        buttonLogout.setVisibility(View.INVISIBLE);
        cardViewAbsen.setVisibility(View.INVISIBLE);
        imageViewLogoupi.setVisibility(View.INVISIBLE);
        constraintLayoutGagalKonek.setVisibility(View.VISIBLE);
        imageViewNoConnection.setVisibility(View.VISIBLE);
        buttonUlangi.setVisibility(View.VISIBLE);
    }

    private void visibleAll() {
        txtUsername.setVisibility(View.VISIBLE);
        txtGreeting.setVisibility(View.VISIBLE);
        buttonAbsen.setVisibility(View.VISIBLE);
        buttonLogout.setVisibility(View.VISIBLE);
        cardViewAbsen.setVisibility(View.VISIBLE);
        imageViewLogoupi.setVisibility(View.VISIBLE);
        constraintLayoutGagalKonek.setVisibility(View.INVISIBLE);
        imageViewNoConnection.setVisibility(View.INVISIBLE);
        buttonUlangi.setVisibility(View.INVISIBLE);
    }

    private void verifyToken(String dataToken, String username, String password, String kodedosen) {
        String search  = "eyJ0e";
        if (dataToken.toLowerCase().contains(search.toLowerCase())){
            getUser(dataToken,username,password, kodedosen);
        }else{
            Intent intentLogin = new Intent(activityMain, LoginActivity.class);
            startActivity(intentLogin);
            finish();
        }
    }

    private void getUser(final String dataToken, final String username, final String password, String kodedosen) {
        alert();
        StringRequest stringRequestUser = new StringRequest(Request.Method.GET,
                Url.GET_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            myAlertDialog.dismiss();
                            visibleAll();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray nama = jsonObject.getJSONArray("dt_user");
                            String gelarnama = "";
                            String kodedosen = "";
                            for(int i=0;i<nama.length();i++){
                                JSONObject gelar = nama.getJSONObject(i);
                                gelarnama = gelar.getString("GELARNAMA");
                                kodedosen = gelar.getString("KODEDOSEN");
                            }

                            postDataToSQLite(username, password, kodedosen);
                            txtUsername.setText(gelarnama);
                            txtUsername.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myAlertDialog.dismiss();
                        invisibleAll();
                        Log.d("TAG", "Error = "+ error);
                    }
                }){
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+dataToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequestUser);
    }

    private void postDataToSQLite(String username, String password, String kodedosen){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setKodedosen(kodedosen);
        if (dbHandler.checkUser(username)){
            dbHandler.updateUser(user);
        } else {
            dbHandler.addUser(user);
        }
    }

    private void logoutConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activityMain,R.style.todoDialogLight);
        builder.setTitle("Apakah anda yakin ingin keluar?")
                .setMessage("halaman login ditampilkan pada saat program dijalankan")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        logOut();
                    }
                })
                .setNeutralButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Toast.makeText(getApplicationContext(),"Nowey",Toast.LENGTH_LONG).show();
                    }
                });
        builder.create().show();
    }

    private void logOut() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityMain,R.style.todoDialogLight);
        builder.setTitle("")
                .setMessage("Sedang menghubungkan ke server ...!")
                .setCancelable(false);
        myAlertDialog = builder.show();
        int idUser = dbHandler.getLastId();
        User user = dbHandler.getUser(idUser);
        final String username = user.getUsername();
        final String password = user.getPassword();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Url.POST_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            myAlertDialog.dismiss();
                            visibleAll();
                            JSONObject jsonObject = new JSONObject(response);
                            String dataToken = jsonObject.getString("token");
                            verifyResult(dataToken);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myAlertDialog.dismiss();
                        invisibleAll();
                        Toast.makeText(getApplicationContext(),"Koneksi ke Server Gagal",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void verifyResult(final String dataToken) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityMain,R.style.todoDialogLight);
        builder.setTitle("")
                .setMessage("Sedang menghubungkan ke server ...!")
                .setCancelable(false);
        myAlertDialog = builder.show();

        String search  = "eyJ0e";
        if (dataToken.toLowerCase().contains(search.toLowerCase())){
            int idUser = dbHandler.getLastId();
            User user = dbHandler.getUser(idUser);
            dbHandler.deleteUser(user);

            StringRequest stringRequestUser = new StringRequest(Request.Method.GET,
                    Url.GET_LOGOUT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                myAlertDialog.dismiss();
                                visibleAll();
                                JSONObject jsonObject = new JSONObject(response);
                                String dataResult = jsonObject.getString("message");
                                String searchResult = "successfully";
                                if (dataResult.toLowerCase().contains(searchResult.toLowerCase())){
                                    Intent intentLogin = new Intent(activityMain, LoginActivity.class);
                                    startActivity(intentLogin);
                                    finish();
                                }else {
                                    Toast.makeText(getApplicationContext(),"Data Token Salah",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            myAlertDialog.dismiss();
                            invisibleAll();
                            Log.d("TAG", "Error = "+ error);
                        }
                    }){
                @Override
                public Map getHeaders() {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer "+dataToken);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequestUser);
        }else{
            myAlertDialog.dismiss();
            Toast.makeText(getApplicationContext(),"Ada Kesalahan silahkan coba beberapa saat lagi",Toast.LENGTH_LONG).show();
        }
    }
}
