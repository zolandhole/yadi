package com.example.yarud.spotupi.helper;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spotupi.DetilMKActivity;
import com.example.yarud.spotupi.R;
import com.example.yarud.spotupi.model.ListPenugasan;

import java.util.List;

public class PenugasanAdapter extends RecyclerView.Adapter<PenugasanAdapter.ViewHolder>{

    private List<ListPenugasan> listPenugasans;
    private Context context;

    public PenugasanAdapter(List<ListPenugasan> listPenugasans, Context context) {
        this.listPenugasans = listPenugasans;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_penugasan, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ListPenugasan listPenugasan = listPenugasans.get(position);
        holder.textViewNAMAMK.setText(listPenugasan.getNamamk());
        holder.textViewNAMAPST.setText(listPenugasan.getNamapst());
        holder.textViewNAMAKELAS.setText(listPenugasan.getNamakelas());
        holder.textViewSKS.setText(listPenugasan.getSks());
        holder.textViewSMT.setText(listPenugasan.getSmt());
        holder.textViewTHN.setText(listPenugasan.getThn());
        holder.textViewIDMK.setText(listPenugasan.getIdmk());

        holder.buttonDetil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetilMK = new Intent(context, DetilMKActivity.class);
                intentDetilMK.putExtra("IDMK",listPenugasan.getIdmk());
                context.startActivity(intentDetilMK);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPenugasans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewNAMAMK, textViewNAMAPST, textViewNAMAKELAS, textViewSKS, textViewSMT, textViewTHN, textViewIDMK;
        public Button buttonDetil;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewNAMAMK = itemView.findViewById(R.id.TextViewNAMAMK);
            textViewNAMAPST = itemView.findViewById(R.id.TextViewNAMAPST);
            textViewNAMAKELAS = itemView.findViewById(R.id.TextViewNAMAKELAS);
            textViewSKS = itemView.findViewById(R.id.TextViewSKS);
            textViewSMT = itemView.findViewById(R.id.TextViewSMT);
            textViewTHN = itemView.findViewById(R.id.TextViewTHN);
            textViewIDMK = itemView.findViewById(R.id.TextViewIDMK);
            buttonDetil = itemView.findViewById(R.id.ButtonDetil);
        }
    }
}
