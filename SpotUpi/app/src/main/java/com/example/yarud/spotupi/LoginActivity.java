package com.example.yarud.spotupi;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spotupi.constant.Url;
import com.example.yarud.spotupi.model.DBHandler;
import com.example.yarud.spotupi.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private final AppCompatActivity activityLogin = LoginActivity.this;
    private EditText editTextUsername, editTextPassword;
    private Button buttonLogin;
    private DBHandler dbHandler;
    private AlertDialog myAlertDialog;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        initObjects();
        initListeners();
    }

    private void initViews(){
        editTextUsername = findViewById(R.id.EditTextUsername);
        editTextPassword = findViewById(R.id.EditTextPassword);
        buttonLogin = findViewById(R.id.ButtonLogin);
    }

    private void initListeners(){
        buttonLogin.setOnClickListener(this);
    }

    private void initObjects(){
        user = new User();
        dbHandler = new DBHandler(activityLogin);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ButtonLogin:
                validationForm();
                break;
        }
    }

    private void dialogShow(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityLogin,R.style.todoDialogLight);
        builder.setTitle("")
                .setMessage("Menghubungkan ke Server ...")
                .setCancelable(false);
        myAlertDialog = builder.show();
    }

    private void validationForm() {
        final String username = editTextUsername.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        if (TextUtils.isEmpty(editTextUsername.getText())){
            editTextUsername.setError("NIP harus dimasukan");
        } else if (TextUtils.isEmpty(editTextPassword.getText())){
            editTextPassword.setError("Password perlu dimasukan");
        }else{
            getToken(username, password);
        }
    }

    private void getToken(final String username, final String password) {
        dialogShow();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Url.POST_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            myAlertDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            String dataToken = jsonObject.getString("token");
                            get_User(dataToken, username, password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myAlertDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Silahkan periksa kembali username password anda",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void get_User(final String dataToken, final String username, final String password) {
        dialogShow();
        StringRequest stringRequestUser = new StringRequest(Request.Method.GET,
                Url.GET_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            myAlertDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray nama = jsonObject.getJSONArray("dt_user");
                            String name = "";
                            String kode = "";
                            for(int i=0;i<nama.length();i++){
                                JSONObject gelar = nama.getJSONObject(i);
                                name = gelar.getString("GELARNAMA");
                                kode = gelar.getString("KODEDOSEN");
                            }

                            postDataToSQLite(username, password, kode);
                            Intent main_activity_intent = new Intent(activityLogin, MainActivity.class);
                            main_activity_intent.putExtra("GELARNAMA", name);
                            main_activity_intent.putExtra("KODEDOSEN", kode);
                            startActivity(main_activity_intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myAlertDialog.dismiss();
                        Log.d("TAG", "Error = "+ error);
                    }
                }){
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+dataToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequestUser);
    }

    private void postDataToSQLite(String username, String password, String kode){
        user.setUsername(username);
        user.setPassword(password);
        user.setKodedosen(kode);
        if (dbHandler.checkUser(username)){
            dbHandler.updateUser(user);
        } else {
            dbHandler.addUser(user);
        }
    }
}
