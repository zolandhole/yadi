package com.example.yarud.spotupi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetilMKActivity extends AppCompatActivity {

    private TextView textViewIDMK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_mk);
        textViewIDMK = findViewById(R.id.TextViewIDMK);
        Bundle extras = getIntent().getExtras();
        String detilmk = extras.getString("IDMK");
        textViewIDMK.setText(detilmk);
    }
}
