package com.example.yarud.spot_upi.Model;

public class ModelMataKuliah {
    String idMK ;
    String namaMK;
    String sks;
    String namaKelas;
    String semester;
    String tahunajar;
    String namaPST;

    public void setNamaPST(String namaPST){
        this.namaPST = namaPST;
    }

    public String getNamaPST() {
        return namaPST;
    }

    public ModelMataKuliah(String idMK, String namaMK, String sks, String namaKelas, String semester, String tahunajar, String namaPST){

    }

    public ModelMataKuliah(){

    }

    public void setIdMK(String idMK){
        this.idMK = idMK;
    }

    public void setNamaMK(String namaMK){
        this.namaMK = namaMK;
    }

    public void setSks(String sks){
        this.sks = sks;
    }

    public void setNamaKelas(String namaKelas){
        this.namaKelas = namaKelas;
    }

    public void setSemester(String semester){this.semester = semester;}

    public void setTahunajar(String tahunajar){this.tahunajar = tahunajar;}

    public String getIdMK(){
        return idMK;
    }

    public String getNamaMK(){
        return namaMK;
    }

    public String getSks(){
        return sks;
    }

    public String getNamaKelas(){
        return namaKelas;
    }

    public String getSemester() {return semester;}

    public String getTahunajar() {return tahunajar;}
}
