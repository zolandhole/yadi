package com.example.yarud.spot_upi.Controller;

public class ControllerApi {
    static final String baseUrl = "http://service.siak.upi.edu/index.php/api/siak/";
    public final String urlLogin = "http://api.upi.edu/jwt/api/user/login";
    public final String urlLoginJWT = "http://api.upi.edu/jwt/api/user";
    public final String urlMatakuliah = "http://api.upi.edu/jwt/api/penugasan/";
    public final String urlDetailMatakuliah = "http://api.upi.edu/jwt/api/detil_mk/";
    public final String urlMonitoringPerkuliahan = "http://api.upi.edu/jwt/api/risalah_mk/";
    public final String urlDetailMonitoringPerkuliahan = "http://api.upi.edu/jwt/api/presensi/";
    public final String urlDataAbsensi = "http://api.upi.edu/jwt/api/input_presensi";
}
