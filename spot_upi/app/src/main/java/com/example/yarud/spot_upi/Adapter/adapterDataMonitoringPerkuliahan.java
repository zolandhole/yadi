package com.example.yarud.spot_upi.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spot_upi.Activity.DetailMKActivity;
import com.example.yarud.spot_upi.Activity.DetailMPActivity;
import com.example.yarud.spot_upi.Activity.MonitoringPerkuliahan;
import com.example.yarud.spot_upi.Model.ModelMonitoringPerkuliahan;
import com.example.yarud.spot_upi.R;

import java.util.List;

public class adapterDataMonitoringPerkuliahan extends RecyclerView.Adapter<adapterDataMonitoringPerkuliahan.HolderData>{
    private List<ModelMonitoringPerkuliahan> item;
    private Context context;

    public adapterDataMonitoringPerkuliahan(Context context, List<ModelMonitoringPerkuliahan> item){
        this.item = item;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_monitoring,parent,false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        final Intent intent = new Intent(context, DetailMPActivity.class);
        ModelMonitoringPerkuliahan mdj = item.get(position);
        holder.idrsView.setText(mdj.getIdrs());
        holder.idpnView.setText(mdj.getIdpn());
        holder.pertemuanView.setText(mdj.getPertemuan());
        holder.topikView.setText(mdj.getTopik());
        holder.subtopikView.setText(mdj.getSubtopik());
        holder.sesuaiView.setText(mdj.getSesuai());
        holder.waktuView.setText(mdj.getWaktu());
        holder.useridView.setText(mdj.getUserid());
        holder.approveView.setText(mdj.getApprove());
        intent.putExtra("IDRS",holder.idrsView.getText().toString());
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class HolderData extends RecyclerView.ViewHolder{
        TextView idrsView;
        TextView idpnView;
        TextView pertemuanView;
        TextView topikView;
        TextView subtopikView;
        TextView sesuaiView;
        TextView waktuView;
        TextView useridView;
        TextView approveView;
        Button btnDetail;

        public HolderData (View view){
            super(view);

            idrsView = (TextView) view.findViewById(R.id.TextViewid_rs);
            idpnView = (TextView) view.findViewById(R.id.TextViewid_pn);
            pertemuanView = (TextView) view.findViewById(R.id.TextViewpertemuan);
            topikView = (TextView) view.findViewById(R.id.TextViewtopik);
            subtopikView = (TextView) view.findViewById(R.id.TextViewsubtopik);
            sesuaiView = (TextView) view.findViewById(R.id.TextViewsesuai);
            waktuView = (TextView) view.findViewById(R.id.TextViewwaktu);
            useridView = (TextView) view.findViewById(R.id.TextViewuser_id);
            approveView = (TextView) view.findViewById(R.id.TextViewapprove);
            btnDetail = (Button) view.findViewById(R.id.ButtonPresensi);
        }
    }
}
