package com.example.yarud.spot_upi.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spot_upi.Controller.ApiAuthenticationClient;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClientJWT;
import com.example.yarud.spot_upi.Controller.ApiGetTokenJWT;
import com.example.yarud.spot_upi.Controller.ControllerApi;
import com.example.yarud.spot_upi.Database.DBHandler;
import com.example.yarud.spot_upi.Model.ModelMataKuliah;
import com.example.yarud.spot_upi.Model.User;
import com.example.yarud.spot_upi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailMKActivity extends AppCompatActivity {

    Intent intent;

    TextView txtNamakelas;
    TextView txtNamadosen;
    TextView txtNamamatakuliah;
    TextView txtSks;
    TextView txtNamapst;
    TextView txtTahunajar;
    TextView txtSemesterajar;
    TextView txtHari;
    TextView txtJam1;
    TextView txtJam2;
    TextView txtRuangan;

    Button btnMonitoring;

    ApiGetTokenJWT tokenapi=null;

    Intent intentsenddetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mk);

        intent = getIntent();

        txtNamakelas = (TextView) findViewById(R.id.TextViewDetailNAMAKLS);
        txtNamadosen = (TextView) findViewById(R.id.TextViewDetailNAMADSN);
        txtNamamatakuliah = (TextView) findViewById(R.id.TextViewDetailNAMAMK);
        txtSks = (TextView) findViewById(R.id.TextViewDetailSKS);
        txtNamapst = (TextView) findViewById(R.id.TextViewDetailPST);
        txtTahunajar = (TextView) findViewById(R.id.TextViewDetailTHN);
        txtSemesterajar = (TextView) findViewById(R.id.TextViewDetailSMT);
        txtHari = (TextView) findViewById(R.id.TextViewDetailNAMAHR);
        txtJam1 = (TextView) findViewById(R.id.TextViewDetailJAM1);
        txtJam2 = (TextView) findViewById(R.id.TextViewDetailJAM2);
        txtRuangan = (TextView) findViewById(R.id.TextViewDetailNAMARUANG);
        btnMonitoring = (Button) findViewById(R.id.ButtonRisalah);

        DBHandler db = new DBHandler(this);
        User dbuser = new User(db.getUser(1));
        tokenapi = new ApiGetTokenJWT(DetailMKActivity.this,"3");
        tokenapi.getTokentoServerFirst(dbuser.getUsername(), dbuser.getPassword());

        btnMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentsenddetail = new Intent(DetailMKActivity.this,MonitoringPerkuliahan.class);
                intentsenddetail.putExtra("IDMK",intent.getExtras().getString("IDMK"));
                startActivity(intentsenddetail);
            }
        });


    }

    public void startProccesLogin(String token){
        Log.w("username12", token);
        ApiAuthenticationClientJWT apiAuthenticationClient =
                new ApiAuthenticationClientJWT(
                        new ControllerApi().urlDetailMatakuliah+intent.getExtras().getString("IDMK")
                        , token
                        , "kambinggulingmbe"
                );

        AsyncTask<Void, Void, String> execute = new DetailMKActivity.ExecuteNetworkOperation(apiAuthenticationClient);
        execute.execute();
    }

    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ApiAuthenticationClientJWT apiAuthenticationClient;
        private String isValidCredentials;

        public ExecuteNetworkOperation(ApiAuthenticationClientJWT apiAuthenticationClient) {
            this.apiAuthenticationClient = apiAuthenticationClient;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                isValidCredentials = apiAuthenticationClient.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            findViewById(R.id.loadingPanel).setVisibility(View.GONE);

            Log.w("CREDENTIAL", isValidCredentials);
            // Login Success
            if (isValidCredentials.equals("true")) {
                Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_LONG).show();
                //goToSecondActivity();
            }
            // Login Failed
            else {

                try {
                    JSONArray abcd = new JSONArray(apiAuthenticationClient.getLastResponseAsJsonObject().getJSONArray("dt_mk").toString());
                    for (int i = 0; i < abcd.length(); i++) {
                        JSONObject data = abcd.getJSONObject(i);
                        Log.w("JSON", data.getString("NAMAKELAS"));
                        txtNamakelas.setText(data.getString("NAMAKELAS"));
                        txtNamadosen.setText(data.getString("NAMADSN"));
                        txtNamamatakuliah.setText(data.getString("NAMAMK"));
                        txtSks.setText(data.getString("SKS"));
                        txtNamapst.setText(data.getString("NAMAPST"));
                        txtTahunajar.setText(data.getString("THN"));
                        txtHari.setText(data.getString("NAMAHR"));
                        txtJam1.setText(data.getString("JAM1"));
                        txtJam2.setText(data.getString("JAM2"));
                        txtRuangan.setText(data.getString("NAMARUANG"));
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
