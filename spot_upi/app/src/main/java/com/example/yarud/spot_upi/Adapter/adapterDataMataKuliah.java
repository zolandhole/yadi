package com.example.yarud.spot_upi.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spot_upi.Activity.DetailMKActivity;
import com.example.yarud.spot_upi.Activity.MataKuliah;
import com.example.yarud.spot_upi.Model.ModelMataKuliah;
import com.example.yarud.spot_upi.R;

import java.util.List;

public class adapterDataMataKuliah extends RecyclerView.Adapter<adapterDataMataKuliah.HolderData>{
    private List<ModelMataKuliah> item;
    private Context context;

    public adapterDataMataKuliah(Context context, List<ModelMataKuliah> item){
        this.item = item;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mk,parent,false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        final Intent intent = new Intent(context, DetailMKActivity.class);
        ModelMataKuliah mdj = item.get(position);
        holder.idView.setText(mdj.getIdMK());
        holder.namaPST.setText(mdj.getNamaPST());
        holder.namamatakuliahView.setText(mdj.getNamaMK());
        holder.sksView.setText(mdj.getSks());
        holder.namakelasView.setText(mdj.getNamaKelas());
        holder.semesterView.setText(mdj.getSemester());
        holder.tahunajarView.setText(mdj.getTahunajar());
        intent.putExtra("IDMK",holder.idView.getText().toString());
        intent.putExtra(("KODEMK"),holder.idView.getText().toString());
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class HolderData extends RecyclerView.ViewHolder{
        TextView idView;
        TextView namamatakuliahView;
        TextView sksView;
        TextView namakelasView;
        TextView semesterView;
        TextView tahunajarView;
        TextView namaPST;
        Button btnDetail;

        public HolderData (View view){
            super(view);

            idView = (TextView) view.findViewById(R.id.TextViewIDMK);
            namamatakuliahView = (TextView) view.findViewById(R.id.TextViewDetailNAMAMK);
            namaPST = (TextView) view.findViewById(R.id.TextViewDetailNAMAPST);
            sksView = (TextView) view.findViewById(R.id.TextViewDetailSKS);
            namakelasView = (TextView) view.findViewById(R.id.TextViewDetailNAMAKLS);
            semesterView = (TextView) view.findViewById(R.id.TextViewDetailSMT);
            tahunajarView = (TextView) view.findViewById(R.id.TextViewDetailTHN);
            btnDetail = (Button) view.findViewById(R.id.ButtonRisalahMatakuliah);

        }
    }
}
