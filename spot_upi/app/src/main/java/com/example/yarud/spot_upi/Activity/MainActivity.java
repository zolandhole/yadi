package com.example.yarud.spot_upi.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spot_upi.Controller.ApiAuthenticationClient;
import com.example.yarud.spot_upi.Controller.ApiGetTokenJWT;
import com.example.yarud.spot_upi.Controller.ControllerApi;
import com.example.yarud.spot_upi.Database.DBHandler;
import com.example.yarud.spot_upi.Model.User;
import com.example.yarud.spot_upi.R;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    TextView textnama;
    Button absenbutton;
    Button buttonLogout;
    Intent intent;
    Intent intentsend;
    Intent intentlogout;
    private final AppCompatActivity activityMain = MainActivity.this;

    DBHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        textnama = (TextView) findViewById(R.id.txtUsername);
        intent = getIntent();
        intentlogout = new Intent(this,LoginMenu.class);
        textnama.setText(intent.getExtras().getString("NAMA"));
        absenbutton = (Button) findViewById(R.id.absen_btn);
        buttonLogout = (Button) findViewById(R.id.ButtonLogout);

        db = new DBHandler(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.todoDialogLight);
        builder.setTitle("Logout");
        builder.setMessage("Apakah anda yakin ingin keluar?");
        builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                db.deleteUser(new User(1));
                startActivity(intentlogout);
                finish();
            }
        });

        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        final AlertDialog ad = builder.create();

        absenbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.getExtras().getString("KODEDOSEN");
                intentsend = new Intent(MainActivity.this,MataKuliah.class);
                intentsend.putExtra("KODEDOSEN",intent.getExtras().getString("KODEDOSEN"));
                startActivity(intentsend);
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.show();
            }
        });

    }
}
