package com.example.yarud.spot_upi.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spot_upi.Activity.DetailMPActivity;
import com.example.yarud.spot_upi.Controller.ApiGetTokenJWT;
import com.example.yarud.spot_upi.Model.ModelDetailMonitoringPerkuliahan;
import com.example.yarud.spot_upi.R;

import java.util.List;

public class adapterDetailMonitoringPerkuliahan extends RecyclerView.Adapter<adapterDetailMonitoringPerkuliahan.HolderData>{
    private List<ModelDetailMonitoringPerkuliahan> item;
    private Context context;

    public adapterDetailMonitoringPerkuliahan(Context context, List<ModelDetailMonitoringPerkuliahan> item){
        this.item = item;
        this.context = context;
    }

    @NonNull
    @Override
    public adapterDetailMonitoringPerkuliahan.HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layour_mp,parent,false);
        adapterDetailMonitoringPerkuliahan.HolderData holderData = new adapterDetailMonitoringPerkuliahan.HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(@NonNull final adapterDetailMonitoringPerkuliahan.HolderData holder, int position) {

        String [] keterangan={"Absen","Sakit","Izin"};
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_item, keterangan);
        final ModelDetailMonitoringPerkuliahan mdj = item.get(position);


        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.todoDialogLight);
        builder.setTitle("Update Kehadiran Mahasiswa");
        builder.setMessage("Masukkan Keterangan Ketidakhadiran Mahasiswa");
        holder.inputKet=new EditText(context);
        holder.ketSpinner=new Spinner(context);
        holder.inputKet.setHint("Masukkan Keterangannya");
        holder.ketSpinner=new Spinner(context);
        holder.ketSpinner.setAdapter(adapter);
        builder.setView(holder.ketSpinner);
        //builder.setView(holder.inputKet);
        builder.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DetailMPActivity detailmp = (DetailMPActivity) context;
                ApiGetTokenJWT updateabsen = new ApiGetTokenJWT(context,"5");
                Log.w("USERNAME ADAPTER", detailmp.getUsernameDB());
                holder.rl.setVisibility(View.VISIBLE);
                updateabsen.AbsenToken(detailmp.getUsernameDB(),detailmp.getPasswordDB(),mdj.getIdrs(),mdj.getNim(),"0",holder.ketSpinner.getSelectedItem().toString());
                holder.rl.setVisibility(View.GONE);
                holder.hadirView.setVisibility(View.VISIBLE);
                holder.hadirView.setText(holder.ketSpinner.getSelectedItem().toString());
                holder.inputKet.getText().clear();
                Log.w("KETERANGAN GET", mdj.getKeterangan());
                Log.w("KETERANGAN TOSTRING", holder.inputKet.getText().toString());
                Toast.makeText(context,"Absensi berhasil diupdate",Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                holder.absenSwitch.setChecked(true);
                dialogInterface.dismiss();
            }
        });
        final AlertDialog ad = builder.create();

        holder.ketEdit.setFocusable(true);
        holder.nimView.setText(mdj.getNim());
        holder.namaView.setText(mdj.getNama());
        holder.ketEdit.setVisibility(View.GONE);
        holder.absenButton.setVisibility(View.GONE);
        if(mdj.getStatus().equals("1")){
            holder.hadirView.setVisibility(View.GONE);
            holder.absenSwitch.setChecked(true);
        }else if(mdj.getStatus().equals("0")){
            holder.absenSwitch.setChecked(false);
            holder.hadirView.setText(mdj.getKeterangan());
        }

        holder.absenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    ad.show();
                }else{
                    DetailMPActivity detailmp = (DetailMPActivity) context;
                    ApiGetTokenJWT updateabsen = new ApiGetTokenJWT(context,"5");
                    Log.w("USERNAME ADAPTER", detailmp.getUsernameDB());
                    holder.rl.setVisibility(View.VISIBLE);
                    updateabsen.AbsenToken(detailmp.getUsernameDB(),detailmp.getPasswordDB(),mdj.getIdrs(),mdj.getNim(),"1","");
                    holder.rl.setVisibility(View.GONE);
                    holder.hadirView.setVisibility(View.GONE);
                }
            }
        });
        holder.absenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailMPActivity detailmp = (DetailMPActivity) context;
                ApiGetTokenJWT updateabsen = new ApiGetTokenJWT(context,"5");
                Log.w("USERNAME ADAPTER", detailmp.getUsernameDB());
                holder.rl.setVisibility(View.VISIBLE);
                updateabsen.AbsenToken(detailmp.getUsernameDB(),detailmp.getPasswordDB(),mdj.getIdrs(),mdj.getNim(),"0",holder.ketEdit.getText().toString());
                holder.rl.setVisibility(View.GONE);
                Toast.makeText(context,"Absensi Berhasil Diupdate",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class HolderData extends RecyclerView.ViewHolder{
        TextView nimView;
        TextView namaView;
        Switch absenSwitch;

        TextView hadirView;
        EditText ketEdit;
        EditText inputKet;
        Button absenButton;
        Spinner ketSpinner;

        RelativeLayout rl;

        private HolderData (View view){
            super(view);
            view.setFocusable(true);
            nimView = view.findViewById(R.id.TextViewPresensi_nim);
            namaView = view.findViewById(R.id.TextViewPresensi_nama);
            absenSwitch = view.findViewById(R.id.SwitchPresensi_status);
            hadirView = view.findViewById(R.id.TextViewPresensi_ket);
            rl = view.findViewById(R.id.loadingPanelMPAbsen);

        }
    }
}
