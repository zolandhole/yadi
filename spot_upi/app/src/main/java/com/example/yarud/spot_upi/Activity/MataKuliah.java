package com.example.yarud.spot_upi.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.yarud.spot_upi.Adapter.adapterDataMataKuliah;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClient;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClientJWT;
import com.example.yarud.spot_upi.Controller.ApiGetTokenJWT;
import com.example.yarud.spot_upi.Controller.ControllerApi;
import com.example.yarud.spot_upi.Database.DBHandler;
import com.example.yarud.spot_upi.Model.ModelMataKuliah;
import com.example.yarud.spot_upi.Model.User;
import com.example.yarud.spot_upi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MataKuliah extends AppCompatActivity {

    Intent intent;
    public Intent nextIntent;
    public String pijitwwae;

    RecyclerView recyclerView;

    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;
    List<ModelMataKuliah> item;

    ApiGetTokenJWT tokenapi = null;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mata_kuliah);
        nextIntent = new Intent(MataKuliah.this,DetailMKActivity.class);
        recyclerView = (RecyclerView) findViewById(R.id.mk_recycle_view);
        pd = new ProgressDialog(this);
        item = new ArrayList<>();

        intent = getIntent();

        DBHandler db = new DBHandler(this);
        User dbuser = new User(db.getUser(1));
        tokenapi = new ApiGetTokenJWT(MataKuliah.this,"2");
        tokenapi.getTokentoServerFirst(dbuser.getUsername(), dbuser.getPassword());

        mManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mManager);
        mAdapter = new adapterDataMataKuliah(this,item);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MataKuliah.this, "Make Toast Kabeh",
//                        Toast.LENGTH_LONG).show();
            }
        });

    }

    public void startProccesLogin(String token){
        Log.w("username12", token);
        ApiAuthenticationClientJWT apiAuthenticationClient =
                new ApiAuthenticationClientJWT(
                        new ControllerApi().urlMatakuliah+intent.getExtras().getString("KODEDOSEN")
                        , token
                        , "kambinggulingmbe"
                );

        AsyncTask<Void, Void, String> execute = new MataKuliah.ExecuteNetworkOperation(apiAuthenticationClient);
        execute.execute();
    }


    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ApiAuthenticationClientJWT apiAuthenticationClient;
        private String isValidCredentials;

        public ExecuteNetworkOperation(ApiAuthenticationClientJWT apiAuthenticationClient) {
            this.apiAuthenticationClient = apiAuthenticationClient;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                isValidCredentials = apiAuthenticationClient.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            findViewById(R.id.loadingPanel).setVisibility(View.GONE);

            Log.w("CREDENTIAL", isValidCredentials);
            // Login Success
            if (isValidCredentials.equals("true")) {
                Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_LONG).show();
                //goToSecondActivity();
            }
            // Login Failed
            else {

                try {
                    JSONArray abcd = new JSONArray(apiAuthenticationClient.getLastResponseAsJsonObject().getJSONArray("dt_penugasan").toString());
                    for (int i = 0; i < abcd.length(); i++) {
                        JSONObject data = abcd.getJSONObject(i);
                        Log.w("JSON", data.getString("IDMK"));
                        ModelMataKuliah md = new ModelMataKuliah();
                        md.setIdMK(data.getString("IDMK"));
                        md.setIdMK(data.getString("IDMK"));
                        md.setNamaMK(data.getString("NAMAMK"));
                        md.setSks(data.getString("SKS"));
                        md.setNamaKelas(data.getString("NAMAKELAS"));
                        md.setSemester(data.getString("SMT"));
                        md.setTahunajar(data.getString("THN"));
                        md.setNamaPST(data.getString("NAMAPST"));
                        item.add(md);
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
