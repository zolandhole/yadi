package com.example.yarud.spot_upi.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarud.spot_upi.Adapter.adapterDataMonitoringPerkuliahan;
import com.example.yarud.spot_upi.Adapter.adapterDetailMonitoringPerkuliahan;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClient;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClientJWT;
import com.example.yarud.spot_upi.Controller.ApiGetTokenJWT;
import com.example.yarud.spot_upi.Controller.ControllerApi;
import com.example.yarud.spot_upi.Database.DBHandler;
import com.example.yarud.spot_upi.Model.ModelDetailMonitoringPerkuliahan;
import com.example.yarud.spot_upi.Model.ModelMataKuliah;
import com.example.yarud.spot_upi.Model.ModelMonitoringPerkuliahan;
import com.example.yarud.spot_upi.Model.User;
import com.example.yarud.spot_upi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailMPActivity extends AppCompatActivity {

    Intent intent;

    RecyclerView recyclerView;

    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;
    List<ModelDetailMonitoringPerkuliahan> item;

    ProgressDialog pd;

    ApiGetTokenJWT tokenapi=null;
    String usernameDB;
    String passwordDB;
    String idrsAbsen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mp);

        intent = getIntent();
        idrsAbsen=intent.getExtras().getString("IDRS");

        DBHandler db = new DBHandler(this);
        User dbuser = new User(db.getUser(1));
        usernameDB = dbuser.getUsername();
        passwordDB = dbuser.getPassword();
        tokenapi = new ApiGetTokenJWT(DetailMPActivity.this,"5");
        tokenapi.getTokentoServerFirst(dbuser.getUsername(), dbuser.getPassword());

        recyclerView = (RecyclerView) findViewById(R.id.mp_recycle_view);
        pd = new ProgressDialog(this);
        item = new ArrayList<>();

        mManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mManager);
        mAdapter = new adapterDetailMonitoringPerkuliahan(this,item);
        recyclerView.setAdapter(mAdapter);
    }

    public void startProccesLogin(String token){
        Log.w("username12", token);
        ApiAuthenticationClientJWT apiAuthenticationClient =
                new ApiAuthenticationClientJWT(
                        new ControllerApi().urlDetailMonitoringPerkuliahan+intent.getExtras().getString("IDRS")
                        , token
                        , "kambinggulingmbe"
                );

        AsyncTask<Void, Void, String> execute = new DetailMPActivity.ExecuteNetworkOperation(apiAuthenticationClient);
        execute.execute();
    }

    public String getUsernameDB(){
        return usernameDB;
    }
    public String getPasswordDB(){
        return passwordDB;
    }


    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ApiAuthenticationClientJWT apiAuthenticationClient;
        private String isValidCredentials;

        public ExecuteNetworkOperation(ApiAuthenticationClientJWT apiAuthenticationClient) {
            this.apiAuthenticationClient = apiAuthenticationClient;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            findViewById(R.id.loadingPanelMP).setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                isValidCredentials = apiAuthenticationClient.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            findViewById(R.id.loadingPanelMP).setVisibility(View.GONE);

            Log.w("CREDENTIAL", isValidCredentials);
            // Login Success
            if (isValidCredentials.equals("true")) {
                Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_LONG).show();
                //goToSecondActivity();
            }
            // Login Failed
            else {

                try {
                    JSONArray abcd = new JSONArray(apiAuthenticationClient.getLastResponseAsJsonObject().getJSONArray("dt_presensi").toString());
                    for (int i = 0; i < abcd.length(); i++) {
                        JSONObject data = abcd.getJSONObject(i);
                        Log.w("JSON", data.getString("nim"));
                        ModelDetailMonitoringPerkuliahan md = new ModelDetailMonitoringPerkuliahan();
                        md.setNim(data.getString("nim"));
                        md.setNama(data.getString("nama"));
                        md.setStatus(data.getString("status"));
                        md.setKeterangan(data.getString("ket"));
                        md.setIdrs(data.getString("id_rs"));
                        item.add(md);
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
