package com.example.yarud.spot_upi.Model;

public class ModelDetailMonitoringPerkuliahan {
    public String nim;
    public String nama;
    public String status;
    public String keterangan;
    public String idrs;

    public ModelDetailMonitoringPerkuliahan(){

    }

    public void setNim(String nim){
        this.nim = nim;
    }

    public void setNama(String nama){this.nama = nama;}

    public void setStatus(String status){
        this.status = status;
    }

    public void setKeterangan(String keterangan){
        this.keterangan = keterangan;
    }

    public void setIdrs(String idrs){this.idrs = idrs;}

    public String getNim(){
        return nim;
    }

    public String getNama() {return nama; }

    public String getStatus(){
        return status;
    }

    public String getKeterangan(){
        return keterangan;
    }

    public String getIdrs(){return idrs;}
}
