package com.example.yarud.spot_upi.Model;

public class User {
    private int iduser;
    private String username="";
    private String password="";
    public User()
    {
    }
    public User(int iddosen, String username, String password)
    {
        this.iduser=iddosen;
        this.username=username;
        this.password=password;

    }

    public User(User user){
        try {
            this.iduser = user.getIduser();
            this.username = user.getUsername();
            this.password = user.getPassword();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public User(int iduser){
        this.iduser=iduser;
    }

    public void setIduser(int iduser){this.iduser = iduser;}
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public int getIduser() {return iduser;}
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
}
