package com.example.yarud.spot_upi.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spot_upi.Activity.DetailMKActivity;
import com.example.yarud.spot_upi.Activity.DetailMPActivity;
import com.example.yarud.spot_upi.Activity.LoginMenu;
import com.example.yarud.spot_upi.Activity.MainActivity;
import com.example.yarud.spot_upi.Activity.MataKuliah;
import com.example.yarud.spot_upi.Activity.MonitoringPerkuliahan;
import com.example.yarud.spot_upi.Model.ModelMonitoringPerkuliahan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApiGetTokenJWT {

    public String tokenJWT="";
    Context context;
    String status;

    public ApiGetTokenJWT(Context context ,String status){
        this.context = context;
        this.status = status;
    }

    public void getTokentoServerFirst(final String usernameJWT, final String passwordJWT){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                new ControllerApi().urlLogin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.w("JSONJWT",response);
                            tokenJWT = jsonObject.getString("token");
                            Log.w("STATUS", status);
                            switch (status){
                                case "1":
                                    getContext1();
                                    break;
                                case "2":
                                    getContext2();
                                    break;
                                case "3":
                                    getContext3();
                                    break;
                                case "4":
                                    Log.w("STATUS 4", status);
                                    getContext4();
                                    break;
                                case "5":
                                    getContext5();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(status.equals("1")) {
                            Toast.makeText(context, "Username/Password anda salah", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(context, "Koneksi ke server terputus", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("username", usernameJWT);
                params.put("password", passwordJWT);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //Function get token for absen
    public void AbsenToken(final String usernameJWT, final String passwordJWT, final String idrs, final String nim, final String status, final String ket){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                new ControllerApi().urlLogin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.w("JSONJWT",response);
                            tokenJWT = jsonObject.getString("token");
                            Log.w("STATUS", status);
                            ConfirmAbsen(tokenJWT,idrs,nim,status,ket);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(status.equals("1")) {
                            Toast.makeText(context, "Username/Password anda salah", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(context, "Koneksi ke server terputus", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("username", usernameJWT);
                params.put("password", passwordJWT);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    //Confirm Absen
    public void ConfirmAbsen(final String token, final String idrs, final String nim, final String status, final String ket){
        Map<String,String> params = new HashMap<>();
        params.put("id_rs", idrs);
        params.put("nim", nim);
        params.put("status", status);
        params.put("ket", ket);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                new ControllerApi().urlDataAbsensi, new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //progressDialog.dismiss();
                        try {
                            Log.w("JSONJWT",response.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(status.equals("1")) {
                            Toast.makeText(context, "Username/Password anda salah", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(context, "Koneksi ke server terputus", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }


    //Get All Context
    public void getContext1(){
        LoginMenu login = (LoginMenu) context;
        login.startProccesLogin(tokenJWT);
    }

    public void getContext2(){
        MataKuliah matkul = (MataKuliah) context;
        matkul.startProccesLogin(tokenJWT);
    }

    public void getContext3(){
        DetailMKActivity detailmk = (DetailMKActivity) context;
        detailmk.startProccesLogin(tokenJWT);
    }

    public void getContext4(){
        Log.w("STATUS getcontext", status);
        MonitoringPerkuliahan monitoring = (MonitoringPerkuliahan) context;
        monitoring.startProccesLogin(tokenJWT);
    }

    public void getContext5(){
        DetailMPActivity detailmp = (DetailMPActivity) context;
        detailmp.startProccesLogin(tokenJWT);
    }

}