package com.example.yarud.spot_upi.Activity;

import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClient;
import com.example.yarud.spot_upi.Controller.ApiAuthenticationClientJWT;
import com.example.yarud.spot_upi.Controller.ApiGetTokenJWT;
import com.example.yarud.spot_upi.Controller.ControllerApi;
import com.example.yarud.spot_upi.Database.DBHandler;
import com.example.yarud.spot_upi.Model.User;
import com.example.yarud.spot_upi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LoginMenu extends AppCompatActivity {


    EditText usernameText;
    EditText passwordText;
    Button loginbutton;

    DBHandler db;
    Intent intent;

    Boolean checkingStatus = false;
    String tokenJWT="";

    ApiGetTokenJWT tokenapi = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        usernameText = (EditText) findViewById(R.id.EditTextUsername);
        passwordText = (EditText) findViewById(R.id.EditTextPassword);
        loginbutton = (Button) findViewById(R.id.ButtonLogin);
        db = new DBHandler(this);
        //db.addUser(new User(1,"yandi","yandi"));
        try {
            List<User> userList = db.getAllUser();

            for (User user : userList) {
                Log.w("response ", String.valueOf(user.getIduser()));
                Log.w("response ", user.getPassword());
                Log.w("response ", user.getUsername());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        getUserdb();

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.w("username11", tokenJWT);
                    tokenapi = new ApiGetTokenJWT(LoginMenu.this,"1");
                    tokenapi.getTokentoServerFirst(usernameText.getText().toString(), passwordText.getText().toString());
                } catch (Exception ex) {

                }
            }
        });
    }

    public void startProccesLogin(String token){
        Log.w("username12", token);
        ApiAuthenticationClientJWT apiAuthenticationClient =
                new ApiAuthenticationClientJWT(
                        new ControllerApi().urlLoginJWT
                        , token
                        , "kambinggulingmbe"
                );

        AsyncTask<Void, Void, String> execute = new ExecuteNetworkOperation(apiAuthenticationClient);
        execute.execute();
    }

    //Get user DB from handler
    public void getUserdb() {
        try {
            User dbuser = new User(db.getUser(1));
            Log.w("username",dbuser.getUsername());
            if (dbuser.getUsername().equals("")) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            } else {
                try {
                    tokenapi = new ApiGetTokenJWT(LoginMenu.this,"1");
                    tokenapi.getTokentoServerFirst(dbuser.getUsername(), dbuser.getPassword());

                    checkingStatus = true;
                } catch (Exception ex) {
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

    }


    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ApiAuthenticationClientJWT apiAuthenticationClient;
        private String isValidCredentials;

        public ExecuteNetworkOperation(ApiAuthenticationClientJWT apiAuthenticationClient) {
            this.apiAuthenticationClient = apiAuthenticationClient;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                isValidCredentials = apiAuthenticationClient.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            findViewById(R.id.loadingPanel).setVisibility(View.GONE);

            Log.w("CREDENTIAL", isValidCredentials);
            // Login Success
            if (isValidCredentials.equals("true")) {
                Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_LONG).show();
                //goToSecondActivity();
            }
            // Login Failedddddddddd
            else {
                Log.w("checking status",checkingStatus.toString());
                db = new DBHandler(LoginMenu.this);
                if (checkingStatus.equals(true)){
                    try {
                        JSONObject abcd = new JSONObject(apiAuthenticationClient.getLastResponseAsJsonObject().getJSONObject("dt_user").toString());
                        Log.w("JSON",abcd.getString("STATUS"));
                        if(abcd.getString("STATUS").equals("DOSEN")){
                            intent = new Intent(LoginMenu.this,MainActivity.class);
                            intent.putExtra("KODEDOSEN",abcd.getString("KODEDOSEN"));
                            intent.putExtra("NAMA",abcd.getString("GELARNAMA"));
                            if(usernameText.getText().toString().trim().length() > 0)
                                db.updateUser(new User(1, usernameText.getText().toString(), passwordText.getText().toString()));
                            startActivity(intent);
                            finish();
                        }else if(abcd.getString("STATUS").equals("MAHASISWA")){

                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                }else if(checkingStatus.equals(false)){
                    try {
                        JSONArray abcd = new JSONArray(apiAuthenticationClient.getLastResponseAsJsonObject().getJSONArray("dt_user").toString());
                        for (int i = 0; i < abcd.length(); i++) {
                            JSONObject data = abcd.getJSONObject(i);
                            Log.w("JSON", data.getString("KODEDOSEN"));
                            intent = new Intent(LoginMenu.this, MainActivity.class);
                            intent.putExtra("KODEDOSEN",data.getString("KODEDOSEN"));
                            intent.putExtra("NAMA",data.getString("GELARNAMA"));
                            Log.w("usernameFALSE", usernameText.getText().toString());
                            db.addUser(new User(1,usernameText.getText().toString(),passwordText.getText().toString()));
                            startActivity(intent);
                            finish();
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }
        }

        private boolean isEmpty(EditText edtText) {
            if (edtText.getText().toString().trim().length() > 0) {
                return false;
            }else{
                return true;
            }
        }
    }


}