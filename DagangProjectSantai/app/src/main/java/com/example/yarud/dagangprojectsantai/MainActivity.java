package com.example.yarud.dagangprojectsantai;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.dagangprojectsantai.url.Url;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextEmail, editTextPassword, editTextPassconf;
    private Button buttonDaftar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextEmail = findViewById(R.id.editText_email);
        editTextPassword = findViewById(R.id.editText_password);
        editTextPassconf = findViewById(R.id.editText_passconf);
        progressDialog = new ProgressDialog(this);
        buttonDaftar = findViewById(R.id.buttonDaftar);

        buttonDaftar.setOnClickListener(this);
    }

    private void daftarkanUser(){
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String passconf = editTextPassconf.getText().toString().trim();

        progressDialog.setMessage("Mendaftarkan User...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String pesanObject = jsonObject.getString("message");
                            String squareStart = pesanObject.replace("{","");
                            String squareEnd = squareStart.replace("}","");
                            String doubleQuote = squareEnd.replace("\"","");
                            String emailObject = doubleQuote.replace("email:","");
                            String passwordObject = emailObject.replace("password:","");
                            String passconfObject = passwordObject.replace("passconf:","");
                            String komaObject = passconfObject.replace(",","\n");

                            Toast.makeText(getApplicationContext(),komaObject,Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("passconf", passconf);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if (view == buttonDaftar){
            daftarkanUser();
        }
    }
}
